package it.nodereader.service;

import java.util.List;

import it.nodereader.model.InsertNodeRequest;
import it.nodereader.model.ModifyNodeRequest;
import it.nodereader.model.Node;
import it.nodereader.model.RetrieveNodeTreeRequest;

public interface NodeService {

	Node retrieveNode(long id) throws Exception;
	
	List<Node> retrieveNodeTree(RetrieveNodeTreeRequest request) throws Exception;
	
	Node insertNode(InsertNodeRequest request) throws Exception;

	Node updateNode(ModifyNodeRequest request) throws Exception;

	void deleteNode(long id) throws Exception;




	
}
