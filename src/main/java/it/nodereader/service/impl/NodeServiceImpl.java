package it.nodereader.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.nodereader.entity.NodeEntity;
import it.nodereader.exception.NodeDeleteException;
import it.nodereader.model.InsertNodeRequest;
import it.nodereader.model.ModifyNodeRequest;
import it.nodereader.model.Node;
import it.nodereader.model.RetrieveNodeTreeRequest;
import it.nodereader.repository.NodeRepository;
import it.nodereader.service.NodeService;

@Service
public class NodeServiceImpl implements NodeService {

	@Autowired
	NodeRepository nodeRepository;
	
	@Autowired
	private ModelMapper modelMapper;	
	
	@Override
	public Node retrieveNode(long id) throws Exception {
		
		
		NodeEntity node = nodeRepository.findById(id).orElse(null);
		Node dtoNode = node != null ? modelMapper.map(node, Node.class) : null;
		
		return dtoNode;
	}

	@Override
	public List<Node> retrieveNodeTree(RetrieveNodeTreeRequest request) throws Exception {
		
		List<Node> nodeList;
		
		if(request.getId() != null) { //if id is not null, get all parents of that id else get all nodes in database
			
		
			nodeList = new ArrayList<Node>();
			
			boolean finished = false;
			Node currentNode = this.retrieveNode(request.getId());
			if(currentNode!=null) {
				nodeList.add(currentNode);
			}
			
			while(!finished) {
				if(currentNode != null && currentNode.getParentId()!=null) {
					Node currentParent = this.retrieveNode(currentNode.getParentId());
					if(currentParent != null) {
						nodeList.add(currentParent);
						currentNode = currentParent;
					}else {
						finished = true;	
					}
				}else {
					finished = true; 
				}
			}
		}else {
			List<NodeEntity> nodeListEntity = nodeRepository.findAll();
			nodeList = !CollectionUtils.isEmpty(nodeListEntity) ? nodeListEntity.stream()
					  .map(user -> modelMapper.map(user, Node.class))
					  .collect(Collectors.toList()) : new ArrayList<Node>();
		}
		
		return nodeList;
	}
	

	@Transactional
	@Override
	public Node insertNode(InsertNodeRequest request) throws Exception {
		
		Node dtoNode = request.getNode();
		
		NodeEntity nodeToSave = modelMapper.map(dtoNode, NodeEntity.class);
		NodeEntity savedNode = nodeRepository.save(nodeToSave);
		Node dtoSavedNode = savedNode != null ? modelMapper.map(savedNode, Node.class) : null;
		
		return dtoSavedNode;
	}

	@Transactional
	@Override
	public Node updateNode(ModifyNodeRequest request) throws Exception {
		
		Node dtoNode = request.getNode();
		
		NodeEntity nodeToSave = modelMapper.map(dtoNode, NodeEntity.class);
		NodeEntity savedNode = nodeRepository.save(nodeToSave);
		Node dtoSavedNode = savedNode != null ? modelMapper.map(savedNode, Node.class) : null;
		
		return dtoSavedNode;
	}
	
	@Transactional
	@Override
	public void deleteNode(long id) throws Exception {
		
		boolean isParent = !CollectionUtils.isEmpty(nodeRepository.findByParentId(id).orElse(null));
		
		if(isParent) {
			throw new NodeDeleteException("Devi eliminare tutti i figli prima di eliminare un genitore.");
		}
		
		nodeRepository.deleteById(id);
	}

	
}
