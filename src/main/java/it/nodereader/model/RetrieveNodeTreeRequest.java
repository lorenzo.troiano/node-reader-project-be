package it.nodereader.model;

public class RetrieveNodeTreeRequest {

	Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
