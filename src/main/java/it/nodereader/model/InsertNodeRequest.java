package it.nodereader.model;

public class InsertNodeRequest {

	Node node;

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}
	
}
