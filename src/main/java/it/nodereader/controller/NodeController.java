package it.nodereader.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.nodereader.exception.NodeDeleteException;
import it.nodereader.model.FrontendListResponse;
import it.nodereader.model.FrontendResponse;
import it.nodereader.model.InsertNodeRequest;
import it.nodereader.model.ModifyNodeRequest;
import it.nodereader.model.Node;
import it.nodereader.model.RetrieveNodeTreeRequest;
import it.nodereader.service.NodeService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/node")
public class NodeController {
	
	@Autowired
	private NodeService nodeService;

	@RequestMapping(value = "/retrieve/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> retrieveNode(@PathVariable("id") long id) throws Exception {
		
		Node response = nodeService.retrieveNode(id);
		
		FrontendResponse<Node> fr = new FrontendResponse<Node>();
		fr.setData(response);
		return new ResponseEntity<FrontendResponse<Node>>(fr, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/retrieveNodeTree", method = RequestMethod.POST)
	public ResponseEntity<?> retrieveNodeTree(@RequestBody RetrieveNodeTreeRequest request) throws Exception {
		
		List<Node> response = nodeService.retrieveNodeTree(request);
		
		FrontendListResponse<Node> fr = new FrontendListResponse<Node>();
		fr.setData(response);
		return new ResponseEntity<FrontendListResponse<Node>>(fr, HttpStatus.OK);
	}		
	
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ResponseEntity<?> insertNode(@RequestBody InsertNodeRequest request) throws Exception {
		
		Node response = nodeService.insertNode(request);
		FrontendResponse<Node> fr = new FrontendResponse<Node>();
		fr.setData(response);
		return new ResponseEntity<FrontendResponse<Node>>(fr, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<?> updateNode(@RequestBody ModifyNodeRequest request) throws Exception {
		
		Node response = nodeService.updateNode(request);
		FrontendResponse<Node> fr = new FrontendResponse<Node>();
		fr.setData(response);
		return new ResponseEntity<FrontendResponse<Node>>(fr, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteNode(@PathVariable("id") long id) throws Exception {
		
		nodeService.deleteNode(id);
		FrontendResponse<Node> fr = new FrontendResponse<Node>();
		return new ResponseEntity<FrontendResponse<Node>>(fr, HttpStatus.OK);
	}
	
    @ExceptionHandler({ Exception.class})
    public ResponseEntity<String> handleException(Exception e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
	

}