package it.nodereader.exception;

public class NodeDeleteException extends Exception {

	public NodeDeleteException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
