package it.nodereader.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import it.nodereader.model.InsertNodeRequest;
import it.nodereader.model.Node;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NodeControllerIT {

	@Autowired
	private TestRestTemplate template;

    @Test
    public void insertNode() throws Exception {
    	
    	InsertNodeRequest request = new InsertNodeRequest();
    	
    	Node node = new Node();
    	node.setName("prova");
    	node.setDescription("descrizione prova");
    	node.setParentId(null);
    	
    	request.setNode(node);
    	
        ResponseEntity<Node> response = template.postForEntity("/insertNode", request, Node.class);
        
    }
}